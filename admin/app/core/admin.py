from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from core.models import *

class AgentAdmin(admin.ModelAdmin):
    list_display = ('at')

class TaskAdmin(admin.ModelAdmin):
    list_display = ('name')

class LogsAdmin(admin.ModelAdmin):
    list_display = ('get_type_display')

class FilesAdmin(admin.ModelAdmin):
    list_display = ('name')

"""
class CountInline(admin.StackedInline):
    model = CountTask
    can_delete = False
    verbose_name_plural = 'Count Task'


# Define a new User admin
class UserAdmin2(UserAdmin):
    inlines = (CountInline, )
"""

# Re-register UserAdmin
#admin.site.unregister(User)
#admin.site.register(User, UserAdmin2)


admin.site.register(Agent)

admin.site.register(Task)
admin.site.register(Logs)
admin.site.register(Files)
admin.site.register(SettingsUser)