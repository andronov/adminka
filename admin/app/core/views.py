# -*- coding: utf-8 -*-
import mimetools
import os
import urllib
import warnings
import datetime
from django.db import transaction
from django.http import HttpResponse, HttpRequest
from django.shortcuts import render, render_to_response

# Create your views here.
from django.template import RequestContext
from django.utils import six
from django.utils.decorators import classonlymethod
from django.utils.encoding import iri_to_uri, force_bytes
from django.views.generic import TemplateView
import itertools
#from core.task import *
from core.models import Agent, Task, Logs, Files, _createHash
import base64
from core.task import type_rd, type_re, type_who, type_rep, type_list, type_file, type_filem


class AgentRequest():

    def __init__(self, request):
        self.id = request.POST.get('id')
        self.type_task = request.POST.get('type_task')
        self.type_agent = request.POST.get('type')
        self.somedata = request.POST.get('somedata')
        if request.FILES:
            self.file = request.FILES['data']
        else:
            self.data = request.POST.get('data')


    """
    def get_id(self):
        return self.id
    def get_type_task(self):
        return self.type_task
    def get_type_agent(self):
        return self.type_agent
    def get_somedata(self):
        return self.somedata
    def get_data(self):
        return self.data
    def get_file(self):
        return self.file
    """
    def get_id(self):
        return self.id
    def get_type_task(self):
        return self.type_task
    def get_type_agent(self):
        return self.type_agent
    def get_somedata(self):
        return self.somedata
    def get_data(self):
        return base64.urlsafe_b64encode(self.data)
    def get_file(self):
        return self.file

    def __getattr__(self, name):
       pass


def proc(request):
        agents = AgentRequest(request)
        id = agents.get_id()
        types = agents.get_type_task()
        type_agent = agents.get_type_agent()
        somedata = agents.get_somedata()
        data = base64.urlsafe_b64decode(agents.get_data().encode("utf-8"))
        file = agents.get_file()

        #Check Agent
        agent = Agent().get_or_create(id, type_agent)

        #Check response
        check = check_logs(agent,types)

        #Find Task
        if check:
            task = Task.find_task(agent)
        else:
            task=None

        #Processing
        data = processing(types, agent, data, task)

        #Response
        resp = AgentResponse(task, file, agent, data, somedata)

        return resp.render()




def processing(types, agent, data, task):
        if int(types) == 0:
            result = type_who(types, agent, data, task)
            return result
        elif int(types) == 1:
            result = type_rep(types, agent, data, task)
            return result
        elif int(types) == 2:
            result = type_list(types, agent, data, task)
            return result
        elif int(types) == 3:
            result = type_file(agent)
            return result
        elif int(types) == 4:
            result = type_filem(agent)
            return result
        elif int(types) == 5:
            Logs().add(types, agent, data)
            result = type_re(task)
            return result
        elif int(types) == 6:
            Logs().add(types, agent, data)
            result = type_rd(task)
            return result


def check_logs(agent,types):
        try:
            log =Logs.objects.get(agent=agent,type=types,status=0)
            if log:
                if Logs.objects.filter(agent=agent,type=types,status=1):
                    pass
                else:
                    Logs.objects.create(task_id=log.task_id, status=1, agent=agent, type=types)
                    return False
        except:
            return  True









class AgentResponse():

    def __init__(self, task, file, agent, data, somedata):
        """
        self.task = task
        self.file = file
        self.agent = agent
        self.data = data
        """
        #base64.urlsafe_b64encode(test)
        self.id = agent.uid
        if task:
            self.type_task = self.type
        else:
            self.type_task = ''
        self.type_agent = agent.type
        self.somedata = somedata
        self.data = data

    def get_id(self):
        return self.id
    def get_type_task(self):
        return self.type_task
    def get_type_agent(self):
        return self.type_agent
    def get_somedata(self):
        return self.somedata
    def get_data(self):
        return self.data
    def get_file(self):
        return self.file

    def dicter(self):
        return urllib.urlencode({
    'id' : self.get_id(),#uid
    'type_task' : self.get_type_task(),
    'type_agent' : self.get_type_agent(),
    'somedata' : self.get_somedata(),
    'data' : base64.urlsafe_b64encode(self.get_data()),
    })

    def render(self):
        response = HttpResponse()

        response.content = self.dicter()

        return response



    def __getattr__(self, name):
       pass



def test(request):
    context = RequestContext(request)

    return render_to_response('test.html', context)
