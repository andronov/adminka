# -*- coding: utf-8 -*-
import os
from django.core.exceptions import ValidationError
from django.utils import unittest
from django.test import TestCase

from django.test.client import Client
from core.models import *


class ModelsTaskTest(TestCase):
    longMessage = True

    def setUp(self):
        self.agent = Agent.objects.create(type=1, country="RU", hostname="test", ip="122.6.43")
        self.task = Task.objects.create(name="Test", agent_type='1', geo='ALL', uids=self.agent.uid, type=1, all_limit=2,
limit=-1)
        self.loaded_task = LoadedTask.objects.create(task=self.task, status=0, agent=self.agent)
        self.loaded_task2 = LoadedTask.objects.create(task=self.task, status=1, agent=self.agent)

    def test_core_task_loaded(self):
        """Test models Task method loaded"""
        self.assertEqual(self.task.loaded(), 1)

    def test_core_task_executed(self):
        """Test models Task method executed"""
        self.assertEqual(self.task.executed(), 1)

    def test_core_task_result_limit(self):
        """Test models Task method result_limit"""
        self.assertEqual(self.task.result_limit(), 2)

    def test_core_task_find_task(self):
        """Test models Task method find_task"""
        self.assertEqual(self.task.find_task(self.agent), self.task)

class ModelsFilesTest(TestCase):
    longMessage = True

    def setUp(self):
        self.agent = Agent.objects.create(type=1, country="RU", hostname="test", ip="122.6.43")
        self.file = open("test.txt", "w+")
        self.myfile = SimpleUploadedFile(self.file.name, self.file.read())
        self.files = Files.objects.create(file=self.myfile, agent=self.agent)

    def test_files_add(self):
        """Test models Files method add"""
        self.assertEqual(self.files.add(self.myfile, self.agent), True)
        os.remove(self.files.file.path)


class ModelsLogsTest(TestCase):
    longMessage = True


    def setUp(self):
        self.agent = Agent.objects.create(type=1, country="RU", hostname="test", ip="122.6.43")
        self.file = str(2) + '_' + str(self.agent) + '_' + str(datetime.datetime.now().strftime("%d%m%Y_%I%M%S") + '.txt')
        self.data = 'test'
        self.myfile = SimpleUploadedFile(self.file, str(self.data))
        self.logs = Logs.objects.create(file=self.myfile, agent=self.agent, type='2')

    def test_logs_add(self):
        """Test models Logs method add"""
        self.assertEqual(self.logs.add('2', self.agent, self.data), True)
        os.remove(self.logs.file.path)

class TypeWhoTest(unittest.TestCase):
    longMessage = True

    def setUp(self):
        self.agent = Agent.objects.create(uid=0, type=1, country="RU", hostname="test", ip="122.6.43")
        self.agent.uid_generate()
        self.task = Task.objects.create(name="Test", agent_type=self.agent.type, geo='ALL', uids=self.agent.uid,
type=0,all_limit=2, limit=-1)
        self.task.data_generate()
        self.client = Client()

    def test_who(self):
        # Issue a GET request.
        response = self.client.post('/handler', {'id': self.agent.uid, 'type': self.agent.type, 'type_task': '0',
'somedata': '1', 'data': 'testText'})

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        #Check that the rendered form-data, id = task.id.
        self.assertEqual(len(response['Content-Disposition: form-data; name="id"']), self.task.id)


class TypeRepTest(unittest.TestCase):
    longMessage = True

    def setUp(self):
        self.agent = Agent.objects.create(uid=0, type=1, country="RU", hostname="test", ip="122.6.43")
        self.agent.uid_generate()
        self.task = Task.objects.create(name="Test", agent_type=self.agent.type, geo='ALL', uids=self.agent.uid,
type=1,all_limit=2, limit=-1)
        self.task.data_generate()
        self.client = Client()

    def test_who(self):
        # Issue a GET request.
        response = self.client.post('/handler', {'id': self.agent.uid, 'type': self.agent.type, 'type_task': '1',
'somedata': '1', 'data': 'testText'})

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        #Check that the rendered form-data, id = task.id.
        self.assertEqual(len(response['Content-Disposition: form-data; name="id"']), self.task.id)

class TypeListTest(unittest.TestCase):
    longMessage = True

    def setUp(self):
        self.agent = Agent.objects.create(uid=0, type=1, country="RU", hostname="test", ip="122.6.43")
        self.agent.uid_generate()
        self.task = Task.objects.create(name="Test", agent_type=self.agent.type, geo='ALL', uids=self.agent.uid,
type=2,all_limit=2, limit=-1)
        self.task.data_generate()
        self.client = Client()

    def test_who(self):
        # Issue a GET request.
        response = self.client.post('/handler', {'id': self.agent.uid, 'type': self.agent.type, 'type_task': '2',
'somedata': '1', 'data': 'TestText'})

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        #Check that the rendered form-data, id = task.id.
        self.assertEqual(len(response['Content-Disposition: form-data; name="id"']), self.task)

class TypeFileTest(unittest.TestCase):
    longMessage = True

    def setUp(self):
        self.agent = Agent.objects.create(uid=0, type=1, country="RU", hostname="test", ip="122.6.43")
        self.agent.uid_generate()
        self.task = Task.objects.create(name="Test", agent_type=self.agent.type, geo='ALL', uids=self.agent.uid, type=3,all_limit=2, limit=-1)
        self.task.data_generate()
        self.file = open("test.txt", "w+")
        self.myfile = SimpleUploadedFile(self.file.name, self.file.read())
        self.files = Files.objects.create(file=self.myfile, agent=self.agent)

        self.client = Client()

    def test_file(self):
        # Issue a GET request.
        response = self.client.post('/handler', {'id': self.agent.uid, 'type': self.agent.type, 'type_task': '3', 'somedata': '1', 'data': self.file})

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        #Check that the rendered form-data, id = task.id.
        self.assertEqual(response['Content-Disposition'], 'attachment; filename=test.txt;')

class TypeFilemTest(unittest.TestCase):
    longMessage = True

    def setUp(self):
        self.agent = Agent.objects.create(uid=0, type=1, country="RU", hostname="test", ip="122.6.43")
        self.agent.uid_generate()
        self.task = Task.objects.create(name="Test", agent_type=self.agent.type, geo='ALL', uids=self.agent.uid, type=4,all_limit=2, limit=-1)
        self.task.data_generate()
        self.file = open("test.txt", "w+")
        self.myfile = SimpleUploadedFile(self.file.name, self.file.read())
        self.files = Files.objects.create(file=self.myfile, agent=self.agent)

        self.client = Client()

    def test_file(self):
        # Issue a GET request.
        response = self.client.post('/handler', {'id': self.agent.uid, 'type': self.agent.type, 'type_task': '4', 'somedata': '1', 'data': self.file})

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        #Check that the rendered form-data, id = task.id.
        self.assertEqual(response['Content-Disposition'], 'attachment; filename=test.txt;')




class TypeReTest(unittest.TestCase):
    longMessage = True

    def setUp(self):
        self.agent = Agent.objects.create(uid=0, type=1, country="RU", hostname="test", ip="122.6.43")
        self.agent.uid_generate()
        self.task = Task.objects.create(name="Test", agent_type=self.agent.type, geo='ALL', uids=self.agent.uid,type=5,all_limit=2, limit=-1)
        self.task.data_generate()
        self.client = Client()

    def test_who(self):
        # Issue a GET request.
        response = self.client.post('/handler', {'id': self.agent.uid, 'type': self.agent.type, 'type_task': '5','somedata': '1', 'data': 'TestText'})

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        #Check that the rendered form-data, id = task.id.
        self.assertEqual(len(response['Content-Disposition: form-data; name="id"']), self.task.id)

class TypeRdTest(unittest.TestCase):
    longMessage = True

    def setUp(self):
        self.agent = Agent.objects.create(uid=0, type=1, country="RU", hostname="test", ip="122.6.43")
        self.agent.uid_generate()
        self.task = Task.objects.create(name="Test", agent_type=self.agent.type, geo='ALL', uids=self.agent.uid, type=6,all_limit=2, limit=-1)
        self.task.data_generate()
        self.client = Client()

    def test_rd(self):
        # Issue a GET request.
        response = self.client.post('/handler', {'id': self.agent.uid, 'type': self.agent.type, 'type_task': '6', 'somedata': '1', 'data': 'TestText'})

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        #Check that the rendered form-data, id = task.id.
        self.assertEqual(len(response['Content-Disposition: form-data; name="id"']), self.task.id)
