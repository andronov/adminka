# -*- coding: utf-8 -*-
import base64
import datetime
import hashlib
import random
import time
from uuid import UUID
import uuid
from django.db import transaction
from django.contrib.auth.models import User, AbstractBaseUser
from django.core.files import File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db import models, connections, connection
from django.db.models import Q, F
from django.utils import timezone
from os.path import basename


def _createHash():
    hash = hashlib.sha1()
    hash.update(str(time.time()))
    return hash.hexdigest()[:41]


def _createUid():
    r_uuid = base64.urlsafe_b64encode(uuid.uuid4().bytes)
    return r_uuid.replace('=', '')[:30]


class SettingsUser(models.Model):
    user = models.OneToOneField(User)
    key = models.CharField(max_length=250, blank=True)
    value = models.CharField(max_length=250, blank=True)


class Agent(models.Model):
    type = models.IntegerField()  # тип агента
    uid = models.CharField(max_length=30, unique=True, default=0)
    country = models.CharField(max_length=2, blank=True)
    hostname = models.CharField(max_length=255, blank=True, null=True)
    ip = models.CharField(max_length=17, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return unicode(self.id)


    def get_or_create(self, uid, type_agent):
        agent, created = Agent.objects.get_or_create(uid=uid,type=type_agent, defaults={'uid': uid, 'type': type_agent})
        return agent

    def uid_generate(self, *args, **kwargs):
        self.uid = _createUid()
        super(Agent, self).save()

    class Meta:
        verbose_name = u"Agent"
        verbose_name_plural = u'Agents'



class Task(models.Model):
    TYPE_WHO = 0
    TYPE_REP = 1
    TYPE_LIST = 2
    TYPE_FILE = 3
    TYPE_FILEM = 4
    TYPE_RE = 5
    TYPE_RD = 6
    TYPE_EM = 7

    TYPE_CHOICES = (
        (TYPE_WHO, 'who'),
        (TYPE_REP, 'rep'),
        (TYPE_LIST, 'list'),
        (TYPE_FILE, 'file'),
        (TYPE_FILEM, 'filem'),
        (TYPE_RE, 're'),
        (TYPE_RD, 'rd'),
        (TYPE_EM, 'em'),
    )
    name = models.CharField(max_length=250)

    agent_type = models.CharField(max_length=255, blank=True)
    geo = models.CharField(max_length=255, blank=True)
    uids = models.CharField(max_length=1255, blank=True)

    type = models.IntegerField(choices=TYPE_CHOICES, blank=True, null=True)
    limit = models.IntegerField(blank=True, default=-1)
    all_limit = models.IntegerField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    data = models.CharField(max_length=255, blank=True, null=True, unique=True, default=0)


    def __unicode__(self):
        return unicode(self.id)

    def loaded(self):
        return Logs.objects.filter(task=self.id, status=0).count()

    def executed(self):
        return Logs.objects.filter(task=self.id, status=1).count()

    def result_limit(self):
        return self.loaded() + self.executed()

    @transaction.atomic()
    def save_custom(self, agent):
        Logs.objects.create(task=self, status=0, agent=agent, type=self.type)
        self.all_limit = self.result_limit()
        super(Task, self).save()
        return self


    @staticmethod
    def find_task(agent):
        try:
            task = Task.objects.filter((Q(geo='ALL') | Q(geo__iexact=agent.country)) &
                                   (Q(all_limit__gt=F('limit')) | Q(limit=-1)) &
                                   (Q(uids__icontains=agent.uid))).order_by('name')[0].save_custom(agent)
            return task
        except:
            pass

    class Meta:
        verbose_name = u"Task"
        verbose_name_plural = u'Task'





def make_upload_path(instance, filename):
    """Generates upload path for FileField"""
    return u"media/%s" % (filename)


class Files(models.Model):
    file = models.FileField(upload_to=make_upload_path, blank=True)
    agent = models.ForeignKey(Agent, related_name='files_agent', blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def name(self):
        return basename(self.file.name)

    @staticmethod
    def find_file(data):
        fls = Files.objects.get(file=data)
        print fls
        return fls

    def add(self, file, agent):
        try:
            myfile = SimpleUploadedFile(file.name, file.read())
            Files.objects.create(agent=agent, file=myfile)
            return True
        except:
            return False


def logs_upload_path(instance, filename):
    """Generates upload path for FileField"""
    return u"logs/%s/%s" % (instance.agent, filename)


class Logs(models.Model):
    STATUS_JOB = 0
    STATUS_ACTIVE = 1

    STATUS_CHOICES = (
        (STATUS_JOB, 'in the work'),
        (STATUS_ACTIVE, 'executed')
    )
    type = models.IntegerField(choices=Task.TYPE_CHOICES, blank=True, null=True)  # type task
    agent = models.ForeignKey(Agent, related_name='logs_agent')
    task = models.ForeignKey(Task, related_name='loaded_task', blank=True)
    status = models.IntegerField(choices=STATUS_CHOICES, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, )
    file = models.FileField(upload_to=logs_upload_path, blank=True)

    def add(self, types, agent, data, task):
        try:
            name = str(types) + '_' + str(agent) + '_' + str(datetime.datetime.now().strftime("%d%m%Y_%I%M%S") + '.txt')
            myfile = SimpleUploadedFile(name, str(data))
            Logs.objects.filter(task=task).update(file=myfile)
            return True
        except:
            return False

    def display_text_file(self):
        return self.file.read()

