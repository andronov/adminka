# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from dashboard.views import *
from django.contrib.auth.views import login, logout


urlpatterns = patterns('',
    url(r'^$', login_required(index), name='index'),
    url(r'^settings', login_required(settings), name='settings'),
    url(r'^agents/(?P<id>.*)', login_required(agents), name='agents'),

    url(r'^task/add', login_required(add_task), name='add_task'),
    url(r'^task-file/add', login_required(add_task_file), name='add_task_file'),
    url(r'^task/(?P<id>.*)/edit/', login_required(edit_task), name='edit_task'),
    url(r'^task/(?P<id>.*)/delete/', login_required(delete_task), name='delete_task'),
    url(r'^task/(?P<id>.*)/info/', login_required(info_task), name='info_task'),
    url(r'^task', login_required(task), name='task'),

    url(r'^logs/(?P<id>.*)/delete/', login_required(delete_logs), name='delete_logs'),
    url(r'^logs/(?P<id>.*)/(?P<ids>.*)/', login_required(logs_view_list), name='logs_view_list'),
    url(r'^logs', login_required(logs), name='logs'),

    url(r'^files/(?P<id>.*)/delete/', login_required(delete_files), name='delete_files'),
    url(r'^files', login_required(files), name='files'),

)
