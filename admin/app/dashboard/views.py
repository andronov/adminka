# -*- coding: utf-8 -*-
import base64
import datetime
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from core.models import *
from dashboard.forms import TaskForm, TaskFormFile


def index(request):
    context = RequestContext(request)
    agents = Agent.objects.all()
    context['agents'] = agents
    context['title'] = 'Home'
    context['active_menu'] = 'index'
    return render_to_response('index.html', context)


def task(request):
    context = RequestContext(request)

    context['tasks'] = Task.objects.all()
    context['title'] = 'Tasks'
    context['active_menu'] = 'Tasks'
    return render_to_response('task.html', context)


def logs(request):
    context = RequestContext(request)

    context['logs'] = Logs.objects.all()
    context['title'] = 'Logs'
    context['active_menu'] = 'Logs'
    return render_to_response('logs.html', context)

def files(request):
    context = RequestContext(request)

    context['files'] = Files.objects.all()
    context['title'] = 'Files'
    context['active_menu'] = 'Files'
    return render_to_response('files.html', context)

def settings(request):
    context = RequestContext(request)
    ruser = request.user
    settings = SettingsUser.objects.filter(user=ruser)

    if request.POST:
       num = request.POST.get('number')

       user = SettingsUser.objects.get(user=ruser.id)
       user.key = 'Number of items per page:'
       user.value = num
       user.save()

       context['title'] = 'Settings'
       context['user'] = ruser
       context['settings'] = settings
       context['active_menu'] = 'Settings'
    else:
       context['title'] = 'Settings'
       context['user'] = ruser
       context['settings'] = settings
       context['active_menu'] = 'Settings'
    return render_to_response('settings.html', context)

def agents(request, id=''):
    context = RequestContext(request)

    if id == '':
        return Http404

    agent = Agent.objects.get(id=id)

    context['result'] = agent
    context['task_history'] = Task.objects.filter(Q(uids__icontains=str(agent.id)))
    context['logs'] = agent.logs_agent
    context['files'] = agent.files_agent
    context['title'] = 'Agent | ' + str(agent.id)
    context['active_menu'] = 'Agents'
    return render_to_response('agents.html', context)

def logs_view_list(request, id="", ids=""):
    context = RequestContext(request)
    log = Logs.objects.get(id=id)

    context['files'] = log.agent.files_agent
    context['log'] = log
    context['title'] = 'Logs | List'
    context['active_menu'] = 'Logs'
    return render_to_response('logs-view-list.html', context)

def info_task(request, id=""):
    context = RequestContext(request)

    if id == '':
        return Http404
    tasks = Task.objects.get(id=id)

    loadeds = LoadedTask.objects.filter(task=tasks)

    context['title'] = 'Task info | ' + str(tasks.id)
    context['active_menu'] = 'Tasks'
    context['task'] = task
    context['loadeds'] = loadeds
    return render_to_response('info-task.html', context)

def edit_task(request, id=""):
    context = RequestContext(request)

    if id == '':
        return Http404

    task = Task.objects.get(id=id)

    context['fieldset'] = TaskForm(instance=task)

    if request.POST:
        task_form = TaskForm(request.POST, instance=task)
        print(task_form.errors)
        if task_form.is_valid():
            task = task_form.save(commit=False)
            task.updated = datetime.datetime.now()
            #task.data_generate()



            context['active_menu'] = 'Tasks'
            context['title'] = 'Edit Task'
            return HttpResponseRedirect('/task/')
        else:
            context['errors'] = True
            context['active_menu'] = 'Tasks'
            context['task'] = task
    else:
        context['active_menu'] = 'Tasks'
        context['title'] = 'Edit Task'
        context['task'] = task
    return render_to_response('edit-task.html', context)

def add_task(request):
    context = RequestContext(request)

    context['fieldset'] = TaskForm()

    if request.POST:
        task_form = TaskForm(request.POST)
        if task_form.is_valid():
            task = task_form.save(commit=False)
            task.all_limit = 0
            task.save()


            task.data_generate()
            task.save()


            return HttpResponseRedirect('/task/')
        else:
            context['errors'] = True
            context['active_menu'] = 'Tasks'
    else:
        context['active_menu'] = 'Tasks'
        context['title'] = 'Add Task'
    return render_to_response('add-task.html', context)

def add_task_file(request):
    context = RequestContext(request)
    ruser =request.user

    lister = ''
    for p in request.POST.getlist('files'):
            file = Files.objects.get(id=p)
            lister += str(file.file) + '\n'
    context['fieldset'] = TaskFormFile(initial={'file_type':lister, 'type':3})

    if request.POST.get('limit'):
        task_form = TaskFormFile(request.POST)
        if task_form.is_valid():
            task = task_form.save(commit=False)
            type = request.POST.get('type')
            task.type = type
            task.save()

            if request.POST.getlist('uids') == []:
                task.uids.clear()
                task.save()
            else:
               task.uids.clear()
               task.save()
               for project in request.POST.getlist('uids'):
                   task.uids.add(project)
                   task.save()


            return HttpResponseRedirect('/task/')
        else:
            context['errors'] = True
            context['active_menu'] = 'Tasks'
    else:
        context['active_menu'] = 'Tasks'
        context['title'] = 'Add Task'
    return render_to_response('add-task.html', context)

def delete_task(request, id=""):
    if id == '':
        return Http404

    task = Task.objects.get(id=id)
    task.delete()

    return HttpResponseRedirect('/task/')

def delete_logs(request, id=""):
    if id == '':
        return Http404

    logs = Logs.objects.get(id=id)
    logs.delete()

    return HttpResponseRedirect('/logs/')

def delete_files(request, id=""):
    if id == '':
        return Http404

    files = Files.objects.get(id=id)
    files.delete()

    return HttpResponseRedirect('/files/')