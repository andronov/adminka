# -*- coding: utf-8 -*-
from django import forms
from core.models import *

class TaskForm(forms.ModelForm):

    class Meta:
        model = Task
        exclude = ['created', 'updated', 'all_limit', 'data']

    def __init__(self, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'vTextField'
        #self.fields['agent_type'].widget.attrs['class'] = 'vIntegerField'
        self.fields['type'].widget.attrs['class'] = 'vIntegerField'
        #self.fields['uids'].widget.attrs['class'] = 'vIntegerField'
        self.fields['limit'].widget.attrs['class'] = 'vIntegerField'

class TaskFormFile(forms.ModelForm):

    class Meta:
        model = Task
        exclude = ['created', 'updated', 'all_limit', 'data']

    def __init__(self, *args, **kwargs):
        super(TaskFormFile, self).__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'vTextField'
        #self.fields['agent_type'].widget.attrs['class'] = 'vIntegerField'
        self.fields['type'].widget.attrs['class'] = 'vIntegerField'
        self.fields['type'].widget.attrs['onmousedown'] = 'return false'
        #self.fields['type'].widget.attrs['disabled'] = ''
        self.fields['file_type'].widget.attrs['readonly'] = ''
        #self.fields['uids'].widget.attrs['class'] = 'vIntegerField'
        self.fields['limit'].widget.attrs['class'] = 'vIntegerField'