from django.conf.urls import patterns, include, url

from django.contrib import admin
from admin import settings

admin.autodiscover()

urlpatterns = patterns('',


    url(r'^', include('dashboard.urls')),
    url(r'^', include('core.urls')),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),

    #(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
    #   'document_root': settings.MEDIA_ROOT}),


    #(r'^logs/(?P<path>.*)$', 'django.views.static.serve', {
    #    'document_root': settings.LOGS_ROOT})
)
